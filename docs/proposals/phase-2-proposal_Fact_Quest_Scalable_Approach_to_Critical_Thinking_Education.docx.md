**Part I**: Detailed Proposal

Summary
=======

Modern high school students with access to the Internet are blessed with
vast information sources and cursed with a flood of sensational and
misleading competition for their attention, resulting in a decline in
critical thinking among High School graduates. In addition, teachers are
taxed by the intensive one-on-one student interactions required to help
students master critical thinking skills. These fractures in our
education system have widened with the advent of COVID19 and the
reduction in the availability of in-person instruction. Disadvantaged
groups without access to reliable Internet are now further disadvantaged
by lack of access to instruction and resources that develop critical
thinking skill mastery.

Our open source, science-driven learning system will help teachers scale
their critical thinking instruction and improve student preparation for
modern careers that increasingly necessitate critical thinking skill
mastery to compete with automation. Our mobile app, reachable by SMS
from any mobile phone, will be accessible to disadvantaged groups
lacking home Internet connectivity, a computer, or even a smartphone.
Fact Quest will broaden critical thinking education opportunities for
*all*.

Team
====

Maria Dyshel, Hobson Lane, and the [[Tangible
AI]{.ul}](https://tangibleai.com/) team have a proven track record of
delivering conversational AI systems for education in the US and around
the world.
[[Maya]{.ul}](https://www.facebook.com/messages/t/Maya.to.Protect), a
gamified conversational assistant, educates Nepalese teenagers about
child trafficking.
[[Cindy]{.ul}](http://cindy.tangibleai.com/demo.html), developed jointly
with [[Gemma Bulos]{.ul}](https://kravislab.cmc.edu/meet-the-team/) at
Claremont McKenna College, empowers students and young professionals to
deal with impostor syndrome. Contributing Researchers involved in the
development of the Fact Quest concept and proposal include [[Karen Boyd
(ML Ethics)]{.ul}](http://karenboyd.org/cv.html) and [[Anne Knowles
(Instructional Systems
architect).]{.ul}](https://www.linkedin.com/in/practicalvisionary/) The
proposed Fact Quest tool builds on existing open source projects and
diverse contributor communities managed by Tangible AI. These
community-driven projects implement state of the art [[cognitive
search]{.ul}](http://docs.qary.ai) and [[fact checking
technology]{.ul}](https://gilab.com/tangibleai/django-qary) for
everyone. The Problem(s)

Detailed Description

The Problem
===========

Fact Quest address two pressing education challenges brought to light by
the the reduction in on-site instructional options for K-12 students
during COVID-19:

1.  Decline in critical thinking skill among High School graduates

2.  Chronic lack of reliable home Internet for disadvantaged students

1. Decline in Critical Thinking Skill
-------------------------------------

-   Critical thinking skill mastery is on the decline among US High
    > School graduates

-   Teachers in struggling schools often have less time to teach reading
    > and writing skills[^1]

-   Remote learning during COVID-19 may accelerate the decline in
    > critical thinking skill[^2]

Attempts to accelerate mastery of critical thinking skills in public
schools have been unsuccessful.[^3] Critical thinking education for
disadvantaged, ESL, and EFL students is particularly challenging.[^4] A
Texas newspaper investigation found that the number of failing K-12
students failing at least one subject nearly doubled in the first six
weeks of 2020 during virtual learning, when compared to previous years.
This decline in learning outcomes can result in a rush to reopen
schools, jeopardizing public health.[^5]

2. Lack of Reliable Internet for Disadvantaged Students
-------------------------------------------------------

-   Many K-12 students lack access to the Internet for remote learning
    > during COVID-19

-   School districts losing track of disadvantaged groups due to lack of
    > Internet access

The Pew Research Center estimates that 46% of lower-income US households
do have a broadband internet connection at home, and 29% of low-income
US *adults* do not have a smartphone.[^6] In many School Districts,
drops in enrollment for the Fall 2020 school year may represent a "lost
generation" of students from disadvantaged homes. Not only will these
students miss out on thinking and socialization skill development during
this crucial period in their cognitive development, they will no longer
be accounted for in budgeting and planning for upcoming school years. In
Arlington, Texas alone, administrators cannot locate thousands of
students that they expected to enroll when schools opened in the Fall.
This may create a chronic underbudgeting of resources for public
schools, particularly in struggling school districts.

Past Attempts to Solve It
=========================

1. Attempts to Improve Critical Thinking Skill Mastery
------------------------------------------------------

In addition to addressing the critical thinking instruction needs of
disadvantaged groups, Fact Quest will also help stem the decline in
critical thinking skills more broadly. Past programs have attempted to
boost critical thinking mastery among advanced students with national
and local programs:

-   National "Advanced Placement" Program available at most High Schools

-   "Gifted Student" programs across most US States and many school
    > districts

-   Explicit critical thinking instruction within a conventional STEM
    > curriculum

-   Cognitive skill instruction independent of subject matter[^7]

Effective techniques for critical thinking skill development have long
been studied and implemented. However, these programs require
significant resources, including one-on-one student-teacher interaction
that cannot be economically scaled. And even among the most advanced
students the success of one-on-one dialog with teachers reinforces
psychology research that suggests students can benefit from slowing down
the information bandwidth of the content they are required to absorb.
Psychologists and neuroscientists point to the fact that the human brain
has evolved to absorb abstract concepts at the bandwidth of spoken and
written natural language reading and writing.

The text-centric UX of SMS messaging for the Fact Quest platform ensures
that students are fed critical thinking skill concepts at a more optimal
information bandwidth. Fact Quest text messages present critical
thinking concepts at the information absorption and active learning rate
of the human brain.[^8] Current research indicates that the information
bandwidth of reading and writing encourages critical thinking,
generalization, and metacognition to a degree that is much less likely
with video-game, mobile app, and button-click interfaces that encourage
reactivity and memorization. The gamified text-centric exercises of Fact
Quest will be crafted by teachers and researchers to target mastery of
critical thinking skills in ways that may not be possible with other
more conventional approaches.

2. Attempts to Provide Students with Internet Access
----------------------------------------------------

Several organizations, including schools, have tried to fill the
Internet accessibility gap by providing computers and Internet access in
creative ways during COVID-19:

-   School bus and Library WiFi hotspots

-   Programs offering free Home Internet access for disadvantaged
    > students

The 8-year old nonprofit Education Super Highway has successfully
ensured that 99% of schools in the US have Internet access and 94% of
schools use it during at least half of their classes. And they recently
launched [[the project "Digital Bridge
K-12"]{.ul}](https://www.techrepublic.com/article/more-than-9-million-students-falling-behind-from-lack-of-internet-connectivity/)
to help schools develop programs to provide students access to the
Internet and computers at home. However, the task is daunting and it may
take years to ensure that all disadvantaged students have access to the
Internet at home. Meanwhile a "lost cohort" of students is slipping
through the cracks. In addition, long term funding for social welfare
programs that address this need is uncertain. Fact Quest will
immediately provide a connection to critical thinking education
resources for those disadvantaged students who may never be able to
access reliable Internet or computers at Concept overview home.

Description of the New Tool
===========================

The Fact Quest application provides a platform for teachers to author
text adventure game content to target specific critical thinking
learning goals for their students. The platform also provides
researchers and teachers with the analytics they need to track the
efficacy of each of these *Quests*. This detailed feedback can be
incorporated into future *Quest* content and design.

-   Increase student *critical thinking* skill mastery with a
    > text-centric learning platform

-   Extend instructor impact on student *critical thinking* skill with
    > *artificial intelligence*

```{=html}
<!-- -->
```
-   Real time visibility into student critical thinking metrics

-   Critical thinking challenges and subject matter customized by
    > teachers for individuals

-   Continuous deployment and monitoring of learning engineering
    > experiments

-   Robust open source community experimenting and continuously
    > optimizing the tool

![](media/image5.png){width="6.5in" height="2.2192552493438322in"}

Teachers and researchers will learn of additional aspects of critical
thinking and collaborate with each other within the Fact Quest platform
to implement these learning goals into a dialog tree. These concepts
will flourish and evolve among the forest of critical thinking
instruction approaches crafted by teachers, researchers, and even
students and parents. In addition to the anonymized analytics from
parents and students, anyone that engages in Quests will have
opportunities to intentionally provide feedback and Quest ideas.
Teachers and researchers can then incorporate those ideas into future
Quests. This feedback can also be used to inform the design of
customized interfaces to the Fact Quest platform that allow students and
parents to directly author their own Quests. Composition of a Quest
could even be the video game "Boss" that students must overcome in order
to demonstrate mastery of the Fact Quest critical thinking game.

Learning Goal
=============

Students will gain mastery of several components of *critical thinking*
when playing Fact Quest games crafted by teachers and researchers. Some
of the aspects of critical thinking that we have already been able
address with example Quests (based on conventional curricula) include:

-   Deductive reasoning

-   Abstraction

-   Lateral thinking

While managing gameplay (student Quests), several finetuned Natural
Language Understanding models (based on the BERT transformer) will
recognize common critical thinking fallacies in order to inform the
dialog with the student. This will help the system measure student
progress as well as provide hints and Socratic guidance to the student.
The Fact Quest platform will enable teachers to craft Quests that help
students master critical thinking by recognizing more than 200 common
logical fallacies listed in various references.[^9] These hints will
also be customizable by teachers and researchers. Some logical fallacies
are so frequently taught in High School that they may merit example
implementations by the Fact Quest developers to "prime the pump" for
teachers:

-   Ad hominem

-   Begging the Question

-   Non Sequitur

-   Straw Man

-   Affirming the Consequent

Research Base for the Tool
==========================

Fact Quest will be instrumented from the ground up to track every
student-app interaction. This will enable the research community to mine
aggregated statistics for new approaches to learning engineering and
craft experiments to investigate their individual research interests.
The research community has long recognized the potential for virtual
environments for critical thinking education and evaluation.[^10] Escape
room challenges involving logical puzzles similar to Fact Quest
challenges have proven effective in teaching critical thining in the
higher education environment.[^11] Studies have also shown the
effectiveness of text-centric mobile apps for learning language
skills.[^12] Fact Quest builds on this research base to extend these
critical thinking education tools to disadvantaged communities using SMS
messaging.

Like modern commercial apps, the Fact Quest app is data-centric,
providing UX designers and researchers with limitless options for
content experimentation. Whereas conventional A/B testing and analytics
tools, such as Optimizely and UpGrade, enable incremental improvement in
the student learning experience, Fact Quest will be configurable with a
GUI similar to best-in-class content management systems such as
WordPress and Wix. By making the customization of text content
accessible to teachers, administrators, and researchers alike, Fact
Quest will parallelize innovation and content creation, accelerating
improvement in learning outcomes.

The self-service approach to content creation and *text-centric* focus
of the UX ensures that Fact Quest can scale to a wide variety of
education settings and student skill levels by automating and
streamlining teacher-student interactions. This frees teachers' time for
the critical task of crafting content that is customized for the
individuals in a particular class. Fact Quest mediates the
student-teacher interaction with AI to coax students with hints and
nudges and provide teachers with suggestions, customized for a
particular student-teacher interaction. The text-centric nature of the
Fact Quest UX takes advantage of recent advances in Natural Language
Understanding technology for real-time customization of the student
experience as well as real-time analytics on research experiments. In
Fact Quest, student mastery of critical thinking and writing skills can
be quantitatively tracked and accelerated.

In addition, Fact Quest is built from the ground up to be infinitely
scalable to any number of students and teachers. Our cloud
infrastructure makes this possible through auto scaling and load
balancing of compute resources. In addition, the data architecture
enables distribution of databases for individual states, school
districts, schools, or even classrooms. The operational costs of Fact
Quest will scale sublinearly despite these data privacy silos, due to
the economies of scale around administration and cloud compute resource
costs for horizontal scaling.

The *text-centric* UX of Fact Quest also ensures that the information
bandwidth presented to students is matched to the optimal information
absorption and active learning rate of the human brain. Current research
indicates that the slower information I/O (input/output) bandwidth of
reading and writing encourages critical thinking, generalization, and
metacognition to a degree that is much less likely with modern
video-game interfaces that encourage reactivity and memorization. The
gamified *text-centric* exercises of Fact Quest will be crafted by
teachers and researchers to target mastery of critical thinking skills
in ways that may not be possible with other more conventional
approaches.

Attention to Equity
===================

By focusing on the accessibility of Fact Quest we hope to begin bridging
the growing education gap for disadvantaged students. The SMS interface
was intentionally chosen as the primary interface for *all* students to
"level the playing field" for both learning and evaluation of critical
thinking skills. Unexpectedly, recent research reveals that the
reduction in information bandwidth of an SMS interface may have benefits
for critical thinking development among the broader student population
as well. Nonetheless the primary focus of the Fact Quest project is to
help students without access to the Internet expand their options for
remote learning during COVID-19 and beyond.

In addition, by automating much of the work of one-on-one
student-teacher instruction and dialog, Fact Quest is a
teacher-multiplier. Fact Quest allows teachers to design a single Quest
that can be shared with thousands of students in the time it would take
for a few one-on-one student teacher conferences to play these "games"
and exercise these critical thinking skills in the classroom. This
enables students in schools with fewer resources and larger
student-teacher ratios to master fundamental critical thinking skills
that form the foundation for all learning.

Plan for Growth
===============

Fact Quest is more than just an app. It's a platform that allows
teachers to compose educational dialog scripts for any kind of Quest
that they can imagine. And it provides those teachers with resources and
suggestions for hundreds of possible Quests useful for critical thinking
education. In the past weeks volunteers have begun prototyping example
Quests such as a lateral thinking puzzle and a creativing writing
Quest.[^13] This experimentation has revealed several kinds of Quests
that are helpful in learning critical thinking skills:

-   Logical Puzzles

-   Yes/No (20-question) Puzzles

-   Child Detective Mysteries (a la Encyclopedia Brown)

-   Skeptical Thinking Exercises

-   Practical Word Problems (requiring mathematical reasoning)

We are proposing to build, not only an app to solve crucial educational
needs, but also a platform that can evolve and grow to meet future
needs. Fact Quest is built on a strong foundation of privacy protections
at the physical, operational, and network security layers. This enables
us to grow the functionality and scale of the Fact Quest platform while
continuing to protect student privacy. For example, researchers gain
access to anonymized data through a process modeled after the
registration process for some of the best-in-class psychology studies
registered with the Center for Open Science. We will also share
anonymized Fact Quest data on this platform quarterly.

Learning Engineering
====================

The primary learning engineering goal for the Fact Quest platform is to
facilitate the design and testing of a text-based critical thinking
learning tool that can scale to large numbers of disadvantaged students
with minimal incremental per-student resources. To that end, all
software and content for the Fact Quest platform is open source,
allowing researchers from around the world to build on our success.

The Fact Quest platform is designed with all K-12 education system
stakeholders in mind: students, teachers, parents, administrators, and
researchers. Fact Quest is instrumented from the ground up to log all
interactions with students and teachers, most-importantly the text
messages they exchange with the system and each other.

While teachers may want only a simplified set of parameters to adjust,
the researcher interface affords complete access to all elements of the
user experience. This enables researchers to compare the effects of
different system parameters at the flip of a switch. Researchers can use
Fact Quest as a testbed to examine effects of various manipulations and
to examine how these manipulations might influence interest, use, and
learning outcomes for various types of learners at any skill level.

One straightforward aspect of critical thinking that can be incorporated
into various Quests is the concept of Implicatures \-- things
communicated without being said. Paul Grice (1913-1988) developed four
maxims present in conversations observing the [[Cooperative
Principle]{.ul}](https://en.wikipedia.org/wiki/Cooperative_principle):
quality, quantity, manner, and relevance. These aspects can be exploited
with intentional affect in the design of dialog for teaching critical
thinking.

Edward M. Glaser listed three elements needed in critical thinking: an
attitude toward thoughtfulness, knowledge of reasoning methods, and
experience in applying the methods.[^14] Fact Quest engages these by
role-modeling inquisitiveness in its assertions, presenting logical
puzzles to the students, and by engaging the student in the practice of
research and investigation and applying deductive reasoning. Once the
student has experience in doing something in the safe environment of
Fact Quest, they can apply that in real-life environments including
their online research.

Lastly, the interactive nature of Fact Quest taps into the [[flipped
classroom]{.ul}](https://en.wikipedia.org/wiki/Flipped_classroom)
strategy, in which the student-chatbot interaction encourages
exploration rather than providing answers upfront. In contrast,
traditional instruction often unintentionally encourages
memorization.[^15] Exploration undoubtedly results in erroneous answers
as well, which, with Fact Quest's feedback, the student may correct, all
while practicing exploration and critical thinking faculties in real
time.

Timeline
========

A fully functioning Fact Quest platform for crafting and monitoring
student gameplay will be deployed and available for use in High Schools
by the summer quarter of 2021. A detailed schedule is provided in the
budget.

[^1]: 1979, Edwards: [[Effective Schools for the Urban
    Poor\*]{.ul}](https://www.midwayisd.org/cms/lib/TX01000662/Centricity/Domain/8/2.%20Edmonds%20Effective%20Schools%20Movement.pdf)

[^2]: 2004, Cavanaugh et al: [[The Effects of Distance Education onK--12
    Student Outcomes: A
    Meta-Analysis]{.ul}](https://eric.ed.gov/?id=ED489533)

[^3]: 2017, Camaratta (Psychology Today): [[The Emerging Crisis in
    Critical
    Thinking]{.ul}](https://www.psychologytoday.com/us/blog/the-intuitive-parent/201703/the-emerging-crisis-in-critical-thinking)

[^4]: 2016, Ferrer et al: [[Designing an EFL Reading Program to Promote
    Literacy Skills, Critical Thinking, and
    Creativity]{.ul}](https://files.eric.ed.gov/fulltext/EJ1119613.pdf)

[^5]: Oct 23, 2020: [[Alarming failure rates among Texas students fuel
    calls to get them back into
    classrooms]{.ul}](https://www.texastribune.org/2020/10/23/texas-students-remote-learning-failing-schools/)

[^6]: 2019, Anderson & Kumar: [[Lower-income Americans still lag in tech
    adoption]{.ul}](https://www.pewresearch.org/fact-tank/2019/05/07/digital-divide-persists-even-as-lower-income-americans-make-gains-in-tech-adoption/)

[^7]: 1986 Herrnstein et al: [[Teaching Thinking
    Skills]{.ul}](https://www.researchgate.net/profile/Raymond_Nickerson/publication/232424806_Teaching_Thinking_Skills/links/564b3d0408ae3374e5dd841b.pdf)

[^8]: 2019, Hutchinson & Barrett: [[The Power of Predictions: An
    Emerging Paradigm for Psychological
    Research]{.ul}](https://journals.sagepub.com/doi/full/10.1177/0963721419831992)

[^9]: [[Encyclopedia of
    Philosophy]{.ul}](https://iep.utm.edu/fallacy/#H2)

[^10]: 2017, Buluc: [[Exercising Critical Thinking in the Online
    Environment]{.ul}](https://www.ceeol.com/search/article-detail?id=540332)

[^11]: 2019, Jambakar et al: [[Benefits of an Escape Room as a Novel
    Educational Activity
    \...]{.ul}](https://medicine.uams.edu/wp-content/uploads/2020/01/UAMS-Escape-Room-Academic-Radiology.pdf)

[^12]: [[Improving Critical Thinking Skills in Mobile
    Learning]{.ul}](https://www.sciencedirect.com/science/article/pii/S1877042809000809/pdf?md5=503b7de384cbf330dc02c3afcf2e3f9a&pid=1-s2.0-S1877042809000809-main.pdf&_valck=1)

[^13]: [[Lateral Thinking Quest demo
    video]{.ul}](http://bit.ly/demolateralthinking) and [[Creative
    Writing Quest demo video]{.ul}](http://bit.ly/democreativewriting)

[^14]: [[Wikipedia -
    Critical_thinking]{.ul}](https://en.wikipedia.org/wiki/Critical_thinking)

[^15]: [[Wikipedia - Flipped
    classroom]{.ul}](https://en.wikipedia.org/wiki/Flipped_classroom)
