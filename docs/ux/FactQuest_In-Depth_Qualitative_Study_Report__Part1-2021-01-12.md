# FactQuest In-Depth Qualitative Study Report: Part 1 - Teachers

by Polina Maguire, UX Researcher 

Stakeholders: 
Hobson Lane, Maria Dyshel

Last updated: January 12, 2020

Goals: Determine the teachers’ engagement level with recognizing/solving the critical thinking skill problem. Determine the main pain points in the current K-12 education process/methodologies in regards to developing students’ critical thinking skills. Understand how the existing product idea/prototype can address these pain points. Explore possible directions/features of the existing product idea in terms of helping teachers address the issue.

Methodology: The user interviews were conducted by me in the form of remote moderated sessions and involved 5 participants relevant to the study.

Participants: 1) ELA teacher, public school, grades 6-8; 2) Social Studies teacher, public school, grades 6-8; 3) Math teacher, alternative education, prior experience in the public school system, grades 6-12; 4) tutor, teaching expert, various subjects, SAT prep; 5) ELA Teacher, public school, grades 9-12.

## Findings

### Teaching methodologies pain points

5 of 5 participants recognized a pressing need for a better way of teaching CT skills
4 of 5 participants pointed out that CT skills are not being taught early enough in the current educational landscape.
2 of 5 participants spoke about the imbalance in favor of literal thinking
2 of 5 participants spoke about focus on content-related tasks without a proper introduction to the instruments and principles of analysis
3 of 5 participants mentioned unhealthy practice for CT tasks mainly emerging at the stage of tests and examinations
3 of 5 participants complained about the lack of cross-curriculum engagement
2 of 5 participants feel that the current approaches don’t take into consideration different types of intelligence (“college-style learner” vs hands-on learner”) which puts students that don’t possess a conventional “college-style” intelligence at disadvantage 
1 out of 5 spoke about schools not being able to effectively incorporate into the education process the out-of-school environment, activities and contexts that teens love 

### System pain points

4 of 5 participants feel that the level of bureaucratic demand for teachers is “ridiculously high”
3 of 5 participants complained that they have to work overtime to meet system requirements
3 of 5 participants said that they look for ways to simplify material load and complexity to timely meet systems requirements
4 of 5 participants said that in the public K-12 system it is impossible to meet the individual needs of different students

### Students pain points (through the eyes of teachers)

5 of 5 participants said that underperforming students usually come from struggling families
4 of 5 participants are convinced that underperforming is a result of lack of time (busy parents are forced to share responsibilities with minors)
4 of 5 participants outlined social anxiety as a common problem with students
4 of 5 participants think that the current system and approaches put students with social and test anxiety at disadvantage
3 of 5 participants mentioned that parents’ involvement in education is one of the key success factors
4 of 5 participants spoke about the cognitive load of the environment and overwhelming amount of distraction that gets in the way of education
1 of 5 participants mentioned that CT tasks are text-based which puts students who struggle with texts at disadvantage (dyslexia, etc.)
4 of 5 participants spoke about advantages of one-on-one teaching for shy students who are reserved in a class setting

### Teaching and learning CT: current tools

Packaged writing programs (Lucy Calkins)

Blums taxonomy: a pyramid of learning that provides test questions, quizzes, etc.

Google Classrom

No online live lessons permitted, only pre-corded ones

No use of chatbots

Closest thing to chatbots – language apps with one-on-one conversations with real people

Quizizz: very popular; engaging to students; runs on its own machine which allows students to succeed even with slow internet; allows teachers to give feedback for wrong answers at the time of placing questions in which lets students retry questions that they got wrong towards the end of the quiz; teachers love this feature because it lets students to learn withing the system in a self-directed way without external help; requires a certain level of understanding of the content area; teachers designing these quizzes need to figure out the common errors and adjust their feedback to these specific errors; labour intensive for teachers.

Kahoot: has become the way that teachers run quizzes; was more exciting at first, now kids find it boring; gives anonymous statistic feedback to the class about how many got it right, what was the right answer, and how many chose each answer; saves from emarassement

### Fact Quest: impressions

In general: Would be great as a warm-up before starting a new topic

In general: Great to excersise argumentative writing (“how can I prove them wrong with the evidence that I have”, “now that you figured out where the 8 cats came from, what could you put together to convince someone else that this is what really happened” ), argument constructor

In general: Smart alternative to a guess-game of multiple-choice tasks

In general: Solves the “I don’t know how to find the answer” problem

In general: Smart alternative to a guess-game of multiple-choice tasks

In general: Solves the “I don’t know how to find the answer” problem

In general: Anonymity, encouragements, guiding prompts and hints, open-ended questions and gamified statistics were listed among the benefits / features to keep

Alliteration: can work well as a remote extension to what was taught in class; practice to keep the new topic fresh; practice for disabled kids or those who took time off due to physical injury – to get caught up

Cats: Not only works for younger students. For ELA can be designed around books, to help analize the story content at intervals as a student goes through the book, with an option to stop, save progress, and come back to it later after more reading/thinking.

Cats: Exercise that part of the brain that asks “why”

Cats: mystery aspect of it feels very engaging

### Fact Quest: using

3 of 5 participants confidently said that they’d use a tool like this in their process

1 of 5 participants said that he wouldn’t use it in the current K-12 bureaucratic landscape 
2 of 5 participants found it difficult to incorporate new tools due to lack of time

1 of 5 participants said that even though she’d use it, she is sceptical about others to be as open to it

3 of 5 participants said that they’d use it even if wasn’t directly linked to their subject content

### Fact Quest: co-designing

3 of 5 participants expressed interest in co-creating flows for this tool

1 of 5 participants said that the more experienced the teacher is the more free and open-minded he’d be about co-creating and using the new tool

1 of 5 participants suggested that teachers could delegate co-designing flows to TAs and/or official teacher’s aide

1 of 5 participants pointed out that the larger the school district is the less top-down instruction is happening there

1 of 5 participants noted that willingness to make time and effort to introduce a new tool depends on the location of the school district

1 of 5 participants thought that younger teachers would be more open and motivated for this

3 of 5 participants said that they’ve alsrwady been doing a lot of teaching content co-creating and they believe it’s already established as part of school teaching routine

2 of 5 participants expressed excitement about being involved in co-designing

1 of 5 participants said that they’d do it if it was a top-down instruction

3 of 5 participants believed that customizing materials/tools is always better than using ready-made one-size-fits-all stuff 

2 of 5 participants said that they will do it if they’d be given extra time

### Teaching and learning CT: general thoughts

Balance between feeding just enough information needed to solve the problem, but directing students as they go to eliminate confusion

Engaging students with questions that are interesting to them to explore and use taxonomies to get them going deeper while finding answers

When students study in a group, group dinamics naturally facilitates critical thinking when more advances kids motivate slower ones to progress faster

Teachers try their best to consider individual needs and capabilities and to customize content and methodologies to make it work

Competitive aspect motivates students

If it’s remote, it should be concise and bite-sized, elaboration takes place in class

Being able to communicate clearly as a measure of success for CT skills

## Next steps

Conducting the second part of user research (high school students, college students)
Generating HMV statements
Ideating the product functionality, features and content that would meet users goals best
