# 2020-09-10 Schmidt Futures Tools Competition -- Proposal Research

## Background

### [tools-competition-launch](https://drive.google.com/file/d/1BLvAAAfGu7tfx-K_0QSTyQegqdWkvO7s/view?usp=sharing)

learning systems with **instrumentation**, **data**, and **research partnerships**... tight feedback loops and **continuous improvements** in how that learning is delivered in **online and blended settings.**"

### [learning-engineering](https://futuresforumonlearning.org/learning-engineering/)

**Learning engineering** ... is **learning science** and **computer science** ...

### [RFP](https://futuresforumonlearning.org/tools-competition/)

1-page proposal due Friday, Sept 18, 2020

#### Example projects (announced at kickoff teleconference)

- [GSoLEN](https://tdlc.ucsd.edu/GSLN/overview.html) - "science-based strategies" to reduce "poverty's impact on learning"
- [Learn Platform](https://learnplatform.com/) - ed tech advice, reviews, and audits
- [UpGrade](https://www.upgrade-platform.org/) - open source, continuous A/B testing for education

## Proposals

### Proposal 1. Critical Thinking Guided Quest

#### Summary

~$20k for a critical thinking tutor bot - a guided web search treasure hunt to teach students how to distinguish fact from fiction.

- mobile (hybrid learning)
- progress score (gamified)
- scores of others (social competition)
- analytics dashboard (data, hybrid learning)
- visualization (gamified)
- NLP hint recommendation engine (AI, scaling)
- hints curated by teacher (IA, scaling)
- peer hints (social collaboration)
- reinforcement learning (continuous improvement)
- debrief blog post (upside down classroom)

Mobile webapp based on Mohammed's prototype for Al Jezera

Django webapp (Mohammed prototype for Al Jezera) that exercises elastic search api with curated documents:

#### Architecture 1: Historical Reddit breaking news


All text could be scraped from Reddit API.
Quest crafted such that it involves an obscure fact for which information is overwhelmed by misinformation.

- Information: Wired Mag, NYT & London Times articles
- Misinformation (to various degrees): WSJ, Fox News, USA Today, Facebook News, Reddit News


 from Wikipedia (perhaps historical Wikipedia articles that had misinformation or promotional text. Formatting of documents would be plain text, to avoid visual clues to misinformation.

#### Budget

- $1000 for prizes
- $0 for 100 ebooks (donated by author)
- $54 for 1 year of kidblog for 200 students

#### User Testing

- 100 free eBooks (published by Manning Publishing)( will be offered as prizes
- Goal is to expose 1000 students to the app with at least 100 completing the "quest" and receiving the prize
- Test demographic is college students because of the research focus of our research partners at Universities

### Proposal 2. English as a Second Language (ESL) Writing Coach

$15k for an ESL tutor bot - like a grammar corrector and style coach that doesn't give you the answer but helps you work your way towards better wording.
