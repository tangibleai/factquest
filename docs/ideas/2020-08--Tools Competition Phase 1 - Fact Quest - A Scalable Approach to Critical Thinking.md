# FactQuest: A Scalable Approach to Critical Thinking Education

## Summary (a few sentences)

High school students are blessed with vast information sources and cursed with a flood of misinformation.  Teachers are taxed by the intensive one-on-one student interactions required to build critical thinking skills. Our open source, science-driven instructional system will help teachers scale their critical thinking instruction and improve student preparation for STEM careers. Our mobile app, with its SMS interface, will be accessible to disadvantaged groups lacking home Internet connectivity and remote learning hardware or experience. *FactQuest* will broaden education opportunities for all.

## Background (100 words)

Maria Dyshel, Hobson Lane, and the Tangible AI team have a proven track record of delivering conversational AI systems for education in the US and around the world. Maya, a gamified conversational assistant, educates Nepalese teenagers about child trafficking. Similarly Cindy, our conversational AI systems developed jointly with Gemma Bulos at Claremont McKenna College, empowers students and young professionals to deal with impostor syndrome. The proposed *FactQuest* tool builds on an existing open source project used by Tangible AI to demonstrate web search and fact checking AI for journalists at Al Jazeera.

Contributing Researchers: Karen Boyd(ML Ethics), Anne Knowles(Instructional Systems)

## Goals

1. Learning Goal: How will the proposal accelerate the rate of improvement of student outcomes? Please aim to address one the following problem areas.
    - Increase the number of students who are reading by 3rd grade
    + Increase the number of students on track in middle - school math
    + Expand the number of students gaining data and computer science skills in high school
    - Driving more students into college through academic and nonacademic supports.
    - You are also welcome to identify another learning goal. If you select your own, please describe why it is important and how it is related to COVID - 19.

*FactQuest* will expand *middle schooler math skill* and confidence while introducing them to *computer science* concepts such as variables, databases, and conditional expressions. The Simple SMS interface will make these concepts approachable to students that have never before had access to a smart phone, computer, or programmable calculator. And it will be fun! In the spirit of _Encyclopedia Brown_ and _Nancy Drew_ children's mystery novels, students will be prompted with a story and asked to use the *FactQuest* search engine to solve the mystery.

Teachers, assisted by the recommendation engine of *FactQuest*, will collaborate to create customized Quests that invoke curiosity and experimenting and "hacking" instinct inherent in middle schoolers. For many Quests the answer key will be provided in the form of a Python expression that can be "gamed" by the student through trial and error substitution of variables in their mind.

# Quest Example

After being given preliminary instructions and examples of what a successful quest looks like students will be prompted with instructions for a particular quest:

FactQuest: "You read the following headline on Facebook today ..."
FactQuest: "**TIL: FDR was the only President to serve 3 terms** ..."
FactQuest: "Is this statement True or False?  Can it be both? ..."
FactQuest: "Your quest, should you chose to accept it, is to populate your investigor notebook with the following facts ..."
FactQuest: "maximum_legal_term_limit_for_US_President = <INTGEGER>"
FactQuest: us_president_term_duration_in_years = <INTEGER >
FactQuest: years_FDR_served_as_president = <INTEGER >
FactQuest: FDR_served_3_terms = [True | False]
FactQuest: answer_to_question = [True | False | both]

Student: Who was FDR?
FactQuest: "Franklin Delano Roosevelt, often referred to by his initials FDR, was an American politician ...""
FactQuest: "... who served as the 32nd president of the United States from 1933 until his death in 1945."
Student: years_Lincoln_served_as_president = 12
FactQuest: Excellent! Keep going!
Student: us_president_term_duration_in_years = 4
FactQuest: You're right! How did you know? You didn't ask me anything about that... maybe you're using the Internet...
...

Eventually the bot would provide the appropriate amount of encouragement and suggestions to help the student think critically about the question, and even find ambiguities in the wording and assumptions of the question. Did you think about presidents in other countries?

# Example *Quest*s and Computational Thinking Hints:

"Who broke the tie in the vote on the 19th ammendment to the US constitution?"
"Who killed JFK?"
"How many neurons are there in the Human Brain?"
"What animal has the largest brain in number of neurons?"
"How old was Abraham Lincoln's son Edward when Eddie died?"

For that last question, answer computational thinking hints might be provided, sorted here in increasing difficulty:

```
eddie_death_age == 3
# OR
3 <= eddie_death_age < 4
# OR
int(total_years(death_date - birth_date)) == 3
# OR
int(total_seconds(death_date - birth_date) / 60 / 60 / 24 / 365.25) == 3
# OR
3 <= (eddie.death_date - eddie.birth_date).total_seconds() / 60 / 60 / 24 / 365.25 < 4
```

2. Learning Engineering: How is your project architected for rapid experimentation and data - driven continuous improvement? Could researchers use the data to better understand how students learn? Consider this as a potential example: bit.ly/...

We require consent from test users, students, instructors, and guardians(as required by law) to use their learning data to improve their learning outcomes and to share with researchers. Anonymized data, within the constraints of GDPR and child protection and privacy law, will be shared freely with researchers to create a powerful community - driven feedback loop of ever - improving learning outcomes for students.

*FactQuest* will engage students' curiosity through the same storytelling and suspense-building techniques employed by fiction writers. Deviations from Paul Grice's four maxims in his "Cooperative Principle": quality, quantity, manner, and relevance will build suspense and increase engagement and learning.[^Foelske][^Abiola]

Gamification with scores, badges, and teacher feedback will guide students as well as the recommendation engine in improving learning outcomes through continuous optimization of system parameters. Machine learning will allow those parameters to be automatically adjusted with each and every student *Quest*. To increase engagement, the system will encourage experimentation by giving students agency through an open ended conversational AI interface.

[^Abiola]: Abiola, L.L. (2014) "The Effect of Digital Storytelling on Kindergarten Pupils’ Achievement ..."
[^Foelske]: Foelske, M. (2014) "Digital storytelling: the impact on student engagement, motivation and academic learning."

3. Effectiveness: What is your evidence - or theory of impact - to explain why you think your idea will improve learning outcomes?

In addition to the engagement and learning engineering techniques mentioned in the previous section, the "Cognitive Bias Codex" and other logical falacy references will be used to suggest wording and structure for *Quests* with the goal of intriguing students with contradictions to their beliefs.[^Hindes] In addition Socratic questioning will be used to prompt students' thinking when they wander too far froom the learning outcome goals for a particular *Quest*.

*FactQuest* engages curiosity by role - modeling inquisitiveness in its assertions, presenting logical puzzles from computing to the students, and by engaging the student in the practice of research. Answer keys, the Python expressions used to evaluate their answers, will sometimes be revealed to students in order to encourage logical thinking and even trigger student attempts at gaming the system or "hacking" their way to an answer through trial and error substitution of variables. Once the student has experience in doing something in the safe environment of *FactQuest* , they can apply that in more real-life environments with higher stakes, such as standardized tests for college entrance exams.

[^Hindes]: Hindes, Steve (2005). "Think for Yourself!: ..."

4. Equity: How does your proposal address the needs of marginalized student populations?

Our mobile app, with its SMS interface, will be accessible to disadvantaged groups lacking home Internet connectivity and remote learning hardware or experience. *FactQuest* will broaden the education opportunities for all.  The conversational interface will also provide guidance and adapt to eqch individual students' language skill level using the same technology we use in our multilingual chatbot, Maya, which adapts to the language needs of Nepalese teenagers.

5. Scalability: How does your proposal scale?

Tangible AI's open domain question answering system (cognitive search) can automitically injest public resources such as Wikipedia, Online Newspapers, and Guttenberg Project books to create a virtually unlimited set of *Quests* for teachers to choose from. Teachers will also be able to edit existing *Quest*s or utilize Tangible AI's free, open source cognitive search engine to compose and share *Quest*s of their own creation. The *FactQuest* dashboard will enable teachers to chare *Quest*s, and even entire curricula of *Quest*s, or *Epics* within their school or publicly with the broader community. Curated suggestions from forums such as Reddit's /r/TIL (Today I Learned) channel will be provided to teachers as inspiration for creation of new *Quest*s.

Interactions with the mobile web app will scale economically at less than 1/100th cent per message, with the average Quest requiring fewer than 20 interactions and a quarter of a cent of infrastructure cost. SMS interactions will also scale sublinearly with a maximum infrastructure cost of 0.5 cents US per interaction or 10 cents per *Quest*. In many cases, donors may be found to subsidize this cost for disadvantaged groups. Budget from this grant will be allocated to defer the cost of all SMS and web app interactions for up to 1,000 unique students and 10,000 *Quest*s.
