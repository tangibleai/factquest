# Research Notes
by Polina Maguire
2020-11-17

## Access to Internet

https://www.pewresearch.org/fact-tank/2019/05/07/digital-divide-persists-even-as-lower-income-americans-make-gains-in-tech-adoption/

## Cognition & Learning and Natural Language

* [1988, Collins, Brown, and Newman](https://apps.dtic.mil/dtic/tr/fulltext/u2/a178530.pdf)
## Crowd-Sourced 20-questions: Cognitive Apprentiship: Teaching Reading, Writing and Mathematics
* [Explicit instruction to make decisions based on data](https://www.pnas.org/content/112/36/11199.short)
* [The Power of Predictions: An Emerging Paradigm for Psychological Research](https://journals.sagepub.com/doi/full/10.1177/0963721419831992)
* 2001, Ferrer & Staley: [Designing an EFL Reading Programto Promote Literacy Skills,Critical Thinking, and Creativity](https://files.eric.ed.gov/fulltext/EJ1119613.pdf)
* 2014, Finn et al: [Cognitive Skills, Student Achievement Tests, and Schools](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3954910/) It has long been understood that explicit critical thinking skill instuction can improve learning outcomes.
* 1986 Hernsteinn et al: [Teaching Thinking Skills](https://www.researchgate.net/profile/Raymond_Nickerson/publication/232424806_Teaching_Thinking_Skills/links/564b3d0408ae3374e5dd841b.pdf)
* 2013 MIT report: https://news.mit.edu/2013/even-when-test-scores-go-up-some-cognitive-abilities-dont-1211#.UqnNd5VX4TJ.twitter
* 1986 Effective Schools for the Urban Poor

## Automation of 20-Questions guidance

https://en.akinator.com/

## Bad Debate/Argument Types

Ted mentioned some of these in his High School daughter's class.

* Ad hominym
* Begging the Question
* Red Herring
* Non Sequitur
* Straw Man
* False Dichotomy
* Affirming the Consequent

## [Encyclopedia of Philosophy](https://iep.utm.edu/fallacy/#H2)

- Abusive Ad Hominem
- Accent
- Accentus
- Accident
- Ad Baculum
- Ad Consequentiam
- Ad Crumenum
- Ad Hoc Rescue
* Ad Hominem
- Ad Hominem, Circumstantial
- Ad Ignorantiam
- Ad Misericordiam
- Ad Novitatem
- Ad Numerum
- Ad Populum
- Ad Verecundiam
- Affirming the Consequent
- Against the Person
- All-or-Nothing
- Ambiguity
- Amphiboly
- Anecdotal Evidence
- Anthropomorphism
- Appeal to Authority
- Appeal to Consequence
- Appeal to Emotions
- Appeal to Force
- Appeal to Ignorance
- Appeal to Money
- Appeal to Past Practice
- Appeal to Pity
- Appeal to Snobbery
- Appeal to the Gallery
- Appeal to the Masses
- Appeal to the Mob
- Appeal to the People
- Appeal to the Stick
- Appeal to Traditional Wisdom
- Appeal to Vanity
- Appeal to Unqualified Authority
- Argument from Ignorance
- Argument from Outrage
- Argument from Popularity
- Argumentum Ad ….
- Argumentum Consensus Gentium
- Availability Heuristic
- Avoiding the Issue
- Avoiding the Question
- Bad Seed
- Bald Man
- Bandwagon
* Begging the Question
- Beside the Point
- Biased Generalizing
- Biased Sample
- Biased Statistics
- Bifurcation
- Black-or-White
- Changing the Question
- Cherry-Picking the Evidence
- Circular Reasoning
- Circumstantial Ad Hominem
- Clouding the Issue
- Common Belief
- Common Cause.
- Common Practice
- Complex Question
- Composition
- Confirmation Bias
- Confusing an Explanation with an Excuse
- Conjunction
- Consensus Gentium
- Consequence
- Converse Accident
- Cover-up
- Cum Hoc, Ergo Propter Hoc
- Curve Fitting
- Definist
- Denying the Antecedent
- Digression
- Disregarding Known Science
- Distraction
- Division
- Domino
- Double Standard
- Either/Or
- Equivocation
- Etymological
- Every and All
- Exaggeration
- Excluded Middle
- False Analogy
- False Balance
- False Cause
* False Dichotomy
- False Dilemma
- False Equivalence
- Far-Fetched Hypothesis
- Faulty Comparison
- Faulty Generalization
- Faulty Motives
- Formal
- Four Terms
- Gambler’s
- Genetic
- Group Think
- Guilt by Association
- Hasty Conclusion
- Hasty Generalization
- Heap
- Hedging
- Hooded Man
- Hyperbolic Discounting
- Hypostatization
- Ideology-Driven Argumentation
- Ignoratio Elenchi
- Ignoring a Common Cause
- Ignoring Inconvenient Data
- Improper Analogy
- Incomplete Evidence
- Inconsistency
- Inductive Conversion
- Insufficient Statistics
- Intensional
- Invalid Reasoning
- Irrelevant Conclusion
- Irrelevant Reason
- Is-Ought
- Jumping to Conclusions
- Lack of Proportion
- Line-Drawing
- Loaded Language
- Loaded Question
- Logic Chopping
- Logical Fallacy
- Lying
- Maldistributed Middle
- Many Questions
- Misconditionalization
- Misleading Accent
- Misleading Vividness
- Misplaced Burden of Proof
- Misplaced Concreteness
- Misrepresentation
- Missing the Point
- Mob Appeal
- Modal
- Monte Carlo
- Name Calling
- Naturalistic
- Neglecting a Common Cause
- No Middle Ground
- No True Scotsman
- Non Causa Pro Causa
* Non Sequitur
- Obscurum per Obscurius
- One-Sidedness
- Opposition
- Outrage, Argument from
- Over-Fitting
- Overgeneralization
- Oversimplification
- Past Practice
- Pathetic
- Peer Pressure
- Perfectionist
- Persuasive Definition
- Petitio Principii
- Poisoning the Well
- Popularity, Argument from
- Post Hoc
- Prejudicial Language
- Proof Surrogate
- Prosecutor’s Fallacy
- Prosody
- Quantifier Shift
- Question Begging
- Questionable Analogy
- Questionable Cause
- Questionable Premise
- Quibbling
- Quoting out of Context
- Rationalization
- Red Herring
- Refutation by Caricature
- Regression
- Reification
- Reversing Causation
- Scapegoating
- Scare Tactic
- Scope
- Secundum Quid
- Selective Attention
- Self-Fulfilling Prophecy
- Self-Selection
- Sharpshooter’s
- Slanting
- Slippery Slope
- Small Sample
- Smear Tactic
- Smokescreen
- Sorites
- Special Pleading
- Specificity
- Stacking the Deck
- Stereotyping
* Straw Man
- Style Over Substance
- Subjectivist
- Superstitious Thinking
- Suppressed Evidence
- Sweeping Generalization
- Syllogistic
- Texas Sharpshooter’s
- Tokenism
- Traditional Wisdom
- Tu Quoque
- Two Wrongs do not Make a Right
- Undistributed Middle
- Unfalsifiability
- Unrepresentative Sample
- Unrepresentative Generalization
- Untestability
- Vested Interest
- Victory by Definition
- Willed ignorance
- Wishful Thinking
- You Too
