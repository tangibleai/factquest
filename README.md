# Fact Quest

An SMS chatbot that teaches critical thinking skills for K-12 students.

## Design Documentation

- [example dialog ideas](https://gitlab.com/tangibleai/factquest/-/blob/master/docs/2020-11-17-dialogue-examples.md)
- [prewriting coach UX IDP questions](https://docs.google.com/document/d/1lWJoiMbifsv0-F9oCnESK8hJi-5hZbwVBdUL8y1VAC0/edit?ts=6026af5f)


## Developer install
### Developer install

First retrieve a copy of the source code for `factquest`:

```bash
git clone git@gitlab.com:tangibleai/fact-quest.git
cd fact-quest/
```

Then, install and use the `conda` python package manager within the [Anaconda](https://www.anaconda.com/products/individual#Downloads) software package.

```console
conda update -y -n base -c defaults conda
conda create -y -n factquest 'python>=3.6.5,<3.9'
conda env update -n factquest -f environment.yml
# for Linux/Mac 
source activate factquest
# for Windows
activate factquest
```
